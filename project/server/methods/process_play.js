/*****************************************************************************/
/* ProcessPlay Methods */
/*****************************************************************************/

Meteor.methods({
 '/app/processPlay': function (scenario,npCharacter,npCharacterRequest,playerResponseCode) {

  var user = Meteor.user();

  var score = user.profile.score;

  if (score === undefined) {

   score = 0;

  }

  var resultMessage;

  // Evaluate input elements
  // In this case, results are completely random
  var randomResult = Math.random() < 0.5 ? true : false; // http://stackoverflow.com/questions/9730966/how-to-decide-between-two-numbers-randomly-using-javascript



  if (randomResult === true) {
   resultMessage = "INSERT YOUR OWN PLAY SPECIFIC MESSAGE HERE, you just got a point! You're on your way to being a winner!";
   // If necessary increment user's score
   score ++;
  } else {
   resultMessage = "INSERT YOUR OWN PLAY SPECIFIC MESSAGE HERE, you just lost a point! You're not very good at this...";
   score --;
  }

  // If necessary select a

  // new scenario,
  // var scenarios = Scenario.find ({}).fetch ();

  // character,
  // var npCharacters = NPCharacter.find ({scenarioCodes: randomScenario.objectCode}).fetch ();

  // and request,
  // var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

  // and return them to the template


  Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.score":score}});

   return {randomScenario:true,playResult:resultMessage,newScenario:null,newNPCharacter:null,newNPCharacterRequest:null};

 }

});